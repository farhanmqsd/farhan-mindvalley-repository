package com.example.mfarhan.pintrestimages;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class MainActivity extends Activity {

    private SwipeRefreshLayout swipeContainer;
    CustomListAdapter adapter;
    ListView my_listview;

    NetworkResourceLibrary networkresourcelib;
    int cacheSize;
    int maxMemory;
    String jsonurl = "http://pastebin.com/raw/wgkJgazE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Caching Memory Size in KBs
        maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // Use 1/8th of the available memory for default memory cache.
        cacheSize = maxMemory / 8;

        //Call to Network Resource Library with maximum cache size as input
        networkresourcelib = new NetworkResourceLibrary(cacheSize);

        byte[] responseData = networkresourcelib.getRemoteResource(jsonurl, new JsonAsyncResponseHandler(jsonurl));
        if(responseData != null)
        {
            getJsonResponse(responseData);
        }

        ArrayList<String> my_array = new ArrayList<>();
        my_listview = (ListView) findViewById(R.id.lvItems);
        adapter = new CustomListAdapter(MainActivity.this, R.layout.imageslist, my_array, networkresourcelib);
        my_listview.setAdapter(adapter);

        //Pull to Referesh contents of URL
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                byte[] responseData = networkresourcelib.getRemoteResource(jsonurl, new JsonAsyncResponseHandler(jsonurl));
                if(responseData != null)
                {
                    getJsonResponse(responseData);
                }

            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // Floating Button for set Memory Cache Size
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Set Memory Cache Size in KBs");
                builder.setMessage("Default Cache Size is " + cacheSize + "KBs");
                final EditText input = new EditText(MainActivity.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String value = input.getText().toString();
                        if(cacheSize < maxMemory/4) {
                            cacheSize = Integer.parseInt(value);
                            networkresourcelib = new NetworkResourceLibrary(cacheSize);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Not Enough Memory", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

    }



/* Handler Class for handling URL Response
   Get Response in Byte Array and convert into any required format
     Images, JSON or XML
 */

    class JsonAsyncResponseHandler extends ResponseHandlerCache {

        public JsonAsyncResponseHandler(String url) {
            super(url);
        }

        @Override
        public void onSuccessResponse(byte[] responseBody) {
            getJsonResponse(responseBody);
        }

        @Override
        public void onFailureResponse(Throwable error) {
            Log.e("Request Failed", error.getMessage());
        }

    }

    public void processJson(String json)
    {
        ArrayList<String> rsp_array = new ArrayList<>();
        Log.e("Response", json);

        try
        {
            JSONArray jArray = new JSONArray(json);
            for (int i=0; i < jArray.length(); i++)
            {
                try
                {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    // Pulling items from the array
                    JSONObject userObject = oneObject.getJSONObject("user");
                    JSONObject profileimageObject = userObject.getJSONObject("profile_image");
                    String imageurl = profileimageObject.getString("large");
                    rsp_array.add(imageurl);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        adapter.clear();
        adapter.addAll(rsp_array);
        adapter.notifyDataSetChanged();
        // Now we call setRefreshing(false) to signal refresh has finished
        swipeContainer.setRefreshing(false);

    }

    public void getJsonResponse(byte[] response)
    {
        try
        {
            InputStream is = new ByteArrayInputStream(response);
            BufferedReader jsoninputStream = new BufferedReader(new InputStreamReader(is));
            String result="";
            String line;
            while ((line = jsoninputStream.readLine()) != null) {
                result += line;
            }
            is.close();
            processJson(result);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}


