package com.example.mfarhan.pintrestimages;

import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

/**
 * Created by farhan on 8/21/2016.
 *
 * This is the Abstract class to process response recieved from URL
 * Implement its abstract methods to get and process data as required.
 * It extends AsyncHttpResponseHandler library which is a third party library
 * for handling remote remote requests.
 */

public abstract class ResponseHandlerCache extends AsyncHttpResponseHandler {

    String resourceurl;

    public ResponseHandlerCache(String url)
    {
        this.resourceurl = url;
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            onSuccessResponse(responseBody);
            addDataToMemoryCache(resourceurl, responseBody);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

        onFailureResponse(error);
    }

    public void addDataToMemoryCache(String key, byte[] response)
    {
        NetworkResourceLibrary.mMemoryCache.put(key, response);
    }

    public abstract void onSuccessResponse(byte[] responseBody);
    public abstract void onFailureResponse(Throwable error);

}
