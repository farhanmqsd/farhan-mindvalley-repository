package com.example.mfarhan.pintrestimages;

import android.content.Context;
import android.support.v4.util.LruCache;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;


/**
 * Created by MFarhan on 8/20/2016.
 *
 * This class is used as library for Downloading and Caching
 * of Remote resources of any irrespective of resource DataType
 * to insure Scalability
 */

public class NetworkResourceLibrary {

    int maxCacheMemorySize;
    public static LruCache<String, byte[]> mMemoryCache;

    public NetworkResourceLibrary(int max)
    {
        this.maxCacheMemorySize = max;

        mMemoryCache = new LruCache<String, byte[]>(maxCacheMemorySize) {
            @Override
            protected int sizeOf(String key, byte[] response) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return response.length/1024;
            }
        };

    }

    public byte[] getRemoteResource(String resourceurl, ResponseHandlerCache responsehandler)
    {
        byte[] cachedresponse = null;

        if(getDataFromMemCache(resourceurl) == null)
        {
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(resourceurl, responsehandler);
            Log.e("Url Caching", "Data NOT Cached");
        }
        else
        {
            cachedresponse = getDataFromMemCache(resourceurl);
            Log.e("Url Caching", "Data IS Cached");
        }

        return cachedresponse;
    }

    public void cancelRequest(Context ctx)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.cancelRequests(ctx, true);
    }

    public byte[] getDataFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

}
