package com.example.mfarhan.pintrestimages;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * Created by MFarhan on 8/19/2016.
 *
 * Class for displaying Images in ListView from URL
 */

public class CustomListAdapter extends ArrayAdapter<String> {

    private ArrayList<String> listData;
    Context context;
    int layoutResourceId;
    NetworkResourceLibrary networkresourcelib;

    public CustomListAdapter(Context context, int resource, ArrayList<String> imglist, NetworkResourceLibrary networkresourcelib) {
        super(context, resource, imglist);

        this.layoutResourceId = resource;
        this.context = context;
        this.listData = imglist;
        this.networkresourcelib = networkresourcelib;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public String getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        View row = convertView;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
        }

        final ImageView profileimage = (ImageView) row.findViewById(R.id.icon);
        final String imageurl = getItem(position);

        profileimage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Animation rotateimg = AnimationUtils.loadAnimation((Activity) context, R.anim.rotateimage);
                profileimage.startAnimation(rotateimg);

            }
        });

        byte[] responseData = networkresourcelib.getRemoteResource(imageurl, new ImagesAsyncResponseHandler(imageurl, profileimage));
        if(responseData != null)
        {
            setImageBitmap(responseData, profileimage);
        }

        return row;
    }


/* Handler Class for handling URL Response
   Get Response in Byte Array and convert into any required format
     Images, JSON or XML
 */

    class ImagesAsyncResponseHandler extends ResponseHandlerCache {

        ImageView imgview;

        public ImagesAsyncResponseHandler(String url, ImageView imv)
        {
            super(url);
            this.imgview = imv;
        }

        public void getImageFromUrl(byte[] response)
        {
            InputStream inputStream = new ByteArrayInputStream(response);
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imgview.setImageBitmap(bitmap);
            }
        }

        @Override
        public void onSuccessResponse(byte[] responseBody) {
            getImageFromUrl(responseBody);
        }

        @Override
        public void onFailureResponse(Throwable error) {
            Log.e("Request Failed", error.getMessage());
        }

    }

    public void setImageBitmap(byte[] response, ImageView imgview)
    {
        InputStream inputStream = new ByteArrayInputStream(response);
        if (inputStream != null) {
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            imgview.setImageBitmap(bitmap);
        }
    }


}
